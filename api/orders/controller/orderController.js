const Order = require("../model/Order");
const Client = require("../../client/model/Client");
const OrderItem = require("../model/OrderItems");
const OrderItemMeta = require("../model/OrderItemMeta");
const WooCommerceAPI = require('woocommerce-api');

exports.getClientOrder = async (req, res) => {
    const id = req.params.id
    console.log(id)
}

exports.getOrdersWordpress = async (req, res) => {
    try {
        var WooCommerce = new WooCommerceAPI({
            url: 'https://bellevie-market.cloud-save.space',
            consumerKey: 'ck_75076b53a3706a6138b914006f94dfdb190d5b99',
            consumerSecret: 'cs_827d54c49267f4f261d5284a3578fda34e636ed0',
            wpAPI: true,
            version: 'wc/v3'
        });
        WooCommerce.getAsync('orders').then(function(result) {
            let json = JSON.parse(result.toJSON().body);
            for (let i in json) {
                const order = new Order({
                    client_id: json[i].customer_id,
                    status: json[i].status,
                    date_created: json[i].date_created,
                    date_end: json[i].date_updated,
                    order_number: json[i].number,
                    total: json[i].total,
                });
                let data = order.save();
                let items = json[i].line_items

                if(items.length > 0) {
                    for(j in items) {
                        const orderItem = new OrderItem({
                            order_item_name: items[j].name,
                            order_item_type: items[j].sku,
                            order_id: order._id
                        })
                        orderItem.save();

                        let itemMeta = items[j]
                        for(k in itemMeta) {
                            let metaExclude = ['variation_id', 'name', 'taxes', 'meta_data', 'parent_name', 'sku', 'total_tax']
                            if(!metaExclude.includes(k)) {
                                const orderItemMeta = new OrderItemMeta({
                                    meta_key: k,
                                    meta_value: itemMeta[k],
                                    order_item_id: orderItem._id
                                })
                                orderItemMeta.save()
                            }
                        }
                    }
                }
                res.status(201).json({ data });
            }
        });
    } catch (err) {
        res.status(400).json({ err: err });
    }
};

exports.findAll = async (req, res) => {
    try {
        Order.find().then(data => {
            console.log(data);
            res.send(data);
        })
            .catch(err => {
                res.status(500).send({
                    message:
                        err.message || "Some error occurred while retrieving tutorials."
                });
            });
    } catch (err) {
        res.status(400).json({ err: err });
    }
};

exports.findOne = (req, res) => {
    console.log(req)
    const id = req.params.id;

    Order.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found Tutorial with id " + id });
            else res.send(data);
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error retrieving Tutorial with id=" + id });
        });
};

exports.findOrderItems = async (req, res) => {
    const id = req.params.id;

    try {
        OrderItem.find({order_id: id})
            .then(data => {
                console.log(data);
                res.send(data);
            })
            .catch(err => {
                res.status(500).send({
                    message:
                        err.message || "Some error occurred while retrieving tutorials."
                });
            });
    } catch (err) {
        res.status(400).json({ err: err });
    }
};

exports.findOrderItemMeta = async (req, res) => {
    const id = req.params.id;
    console.log(id)
    try {
        OrderItemMeta.find({order_item_id: id})
            .then(data => {
                console.log(data);
                res.send(data);
            })
            .catch(err => {
                res.status(500).send({
                    message:
                        err.message || "Some error occurred while retrieving tutorials."
                });
            });
    } catch (err) {
        res.status(400).json({ err: err });
    }
};

exports.getOrderMonth = async (req, res) => {
    try {
        Order.find().then(data => {
            console.log(data);
            res.send(data);
        })
            .catch(err => {
                res.status(500).send({
                    message:
                        err.message || "Some error occurred while retrieving tutorials."
                });
            });
    } catch (err) {
        res.status(400).json({ err: err });
    }
};

exports.createOrder = async (req, res) => {
    try {
        let isOrder = await Order.find({ order_number: req.body.order_number });
        if (isOrder.length >= 1) {
            return res.status(409).json({
                message: "email already in use"
            });
        }
        if (req.body.status === 'wc-processing') {
            var status = 'processing'
        } else if (req.body.status === 'wc-pending') {
            var status = 'pending'
        } else if (req.body.status === 'wc-cancel') {
            var status = 'cancel'
        }
        const order = new Order({
            client_id: req.body.user_id,
            status: status,
            date_created: req.body.date_created,
            date_end: req.body.date_modified,
            order_number: req.body.id,
            total: req.body.total
        });
        let data = order.save();
        console.log(order)
        var WooCommerce = new WooCommerceAPI({
            url: 'https://bellevie-market.cloud-save.space',
            consumerKey: 'ck_75076b53a3706a6138b914006f94dfdb190d5b99',
            consumerSecret: 'cs_827d54c49267f4f261d5284a3578fda34e636ed0',
            wpAPI: true,
            version: 'wc/v3'
        });
    } catch (err) {
        res.status(400).json({ err: err });
    }
};

exports.getOrderMeta = async (req, res) => {
    try {
        var WooCommerce = new WooCommerceAPI({
            url: 'https://bellevie-market.cloud-save.space',
            consumerKey: 'ck_75076b53a3706a6138b914006f94dfdb190d5b99',
            consumerSecret: 'cs_827d54c49267f4f261d5284a3578fda34e636ed0',
            wpAPI: true,
            version: 'wc/v3'
        });
        WooCommerce.getAsync('orders').then(function(result) {
            let json = JSON.parse(result.toJSON().body);
            for (let i in json) {
                let isOrder = Order.find({ order_number: json[i].id });
                let idItem = json[i].id
                let items = json[i].line_items
                if (isOrder.length >= 1) {
                    return res.status(409).json({
                        message: "meta already exist"
                    });
                } else {
                    Order.find({order_number: idItem})
                        .then(data => {
                            for(d in data) {
                                let isOrderMeta = OrderItem.find({order_id: data[d]._id});
                                if (!isOrderMeta.length) {
                                    for (j in items) {
                                        const orderItem = new OrderItem({
                                            order_item_name: items[j].name,
                                            order_item_type: items[j].sku,
                                            order_id: data[d]._id
                                        })
                                        orderItem.save();

                                        let itemMeta = items[j]
                                        for (k in itemMeta) {
                                            let metaExclude = ['variation_id', 'name', 'taxes', 'meta_data', 'parent_name', 'sku', 'total_tax']
                                            if (!metaExclude.includes(k)) {
                                                const orderItemMeta = new OrderItemMeta({
                                                    meta_key: k,
                                                    meta_value: itemMeta[k],
                                                    order_item_id: orderItem._id
                                                })
                                                orderItemMeta.save()
                                            }
                                        }
                                    }
                                }
                            }
                        })
                        .catch(err => {
                            res.status(500).send({
                                message:
                                    err.message || "Some error occurred while retrieving tutorials."
                            });
                        });
                }
            }
        });
    } catch (err) {
        res.status(400).json({ err: err });
    }
};
