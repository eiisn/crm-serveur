const express = require("express");
const router = express.Router();
const auth = require("../../../config/auth");
const orderController = require("../controller/orderController");

router.get("/getwp", orderController.getOrdersWordpress);
router.get("/", orderController.findAll);
router.get("/get/:id", orderController.findOne);
router.get("/get/items/:id", orderController.findOrderItems);
router.get("/get/item/meta/:id", orderController.findOrderItemMeta);
router.get("/get/month", orderController.getOrderMonth);
router.get("/get/:id_wp", orderController.getClientOrder);
router.post("/add", orderController.createOrder);
router.get("/update/meta", orderController.getOrderMeta);

module.exports = router;
