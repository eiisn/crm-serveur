const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const orderItemSchema = mongoose.Schema({
    order_item_name: {
        type: String
    },
    order_item_type: {
        type: String,
    },
    order_id: {
        type: String,
    }
});

const OrderItems = mongoose.model("OrderItems", orderItemSchema);
module.exports = OrderItems;
