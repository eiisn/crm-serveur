const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const orderSchema = mongoose.Schema({
    client_id: {
        type: Number
    },
    status: {
        type: String,
    },
    date_created: {
        type: String,
    },
    date_end: {
        type: Date
    },
    order_number: {
        type: String,
    },
    total: {
        type: Number
    },
});

const Order = mongoose.model("Order", orderSchema);
module.exports = Order;
