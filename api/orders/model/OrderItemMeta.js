const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const orderItemMetaSchema = mongoose.Schema({
     order_item_id: {
        type: String
    },
    meta_key: {
        type: String,
    },
    meta_value: {
        type: String,
    }
});

const OrderItemMeta = mongoose.model("OrderItemMeta", orderItemMetaSchema);
module.exports = OrderItemMeta;
