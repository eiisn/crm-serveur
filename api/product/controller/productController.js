const Product = require("../model/Product");
const WooCommerceAPI = require('woocommerce-api');

exports.getProductsWordpress = async (req, res) => {
    try {
        var WooCommerce = new WooCommerceAPI({
            url: 'https://bellevie-market.cloud-save.space',
            consumerKey: 'ck_75076b53a3706a6138b914006f94dfdb190d5b99',
            consumerSecret: 'cs_827d54c49267f4f261d5284a3578fda34e636ed0',
            wpAPI: true,
            version: 'wc/v2'
        });
        WooCommerce.getAsync('products').then(function(result) {
            let json = JSON.parse(result.toJSON().body);
            console.log(json)
            for (let i in json) {
                const product = new Product({
                    id_wp: json[i].id,
                    name: json[i].name,
                    last_name: json[i].last_name,
                    permalink: json[i].permalink,
                    date_created: json[i].date_created,
                    date_modified: json[i].date_modified,
                    type: json[i].type,
                    status: json[i].status,
                    description: json[i].description,
                    short_description: json[i].short_description,
                    sku: json[i].sku,
                    price: json[i].price,
                    total_sales: json[i].total_sales,
                    stock_status: json[i].stock_status,
                    stock_quantity: json[i].stock_quantity,
                    categories: json[i].categories,
                });
                let data = product.save();
                res.status(201).json({ data });
            }
        });
    } catch (err) {
        res.status(400).json({ err: err });
    }
};

exports.findAll = async (req, res) => {
    try {
        Product.find().then(data => {
            res.send(data);
        })
            .catch(err => {
                res.status(500).send({
                    message:
                        err.message || "Some error occurred while retrieving tutorials."
                });
            });
    } catch (err) {
        res.status(400).json({ err: err });
    }
};

exports.findOne = (req, res) => {
    console.log(req)
    const id = req.params.id;

    Product.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found Tutorial with id " + id });
            else res.send(data);
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error retrieving Tutorial with id=" + id });
        });
};
