const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const clientSchema = mongoose.Schema({
    id_wp: {
        type: Number,
        default: 0
    },
    name: {
        type: String,
        default: ''
    },
    permalink: {
        type: String,
        default: ''
    },
    date_created: {
        type: String,
        default: ''
    },
    date_modified: {
        type: String,
        default: ''
    },
    type: {
        type: String,
        default: ''
    },
    status: {
        type: String,
        default: ''
    },
    description: {
        type: String,
        default: ''
    },
    short_description: {
        type: String,
        default: ''
    },
    sku: {
        type: String,
        default: ''
    },
    price: {
        type: Number,
        default: 0
    },
    total_sales: {
        type: Number,
        default: 0
    },
    stock_status: {
        type: String,
        default: ''
    },
    stock_quantity: {
        type: Number,
        default: 0
    },
    categories: {
        type: Array,
        default: []
    }
});

const Product = mongoose.model("Product", clientSchema);
module.exports = Product;
