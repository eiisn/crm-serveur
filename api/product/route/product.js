const express = require("express");
const router = express.Router();
const productController = require("../controller/productController");

router.get("/getproductwp", productController.getProductsWordpress);
router.get("", productController.findAll);
router.get("/edit/:id", productController.findOne);

module.exports = router;
