const express = require("express");
const router = express.Router();
const clientController = require("../controller/clientController");

router.get("/getuserwordpress", clientController.getUsersWordpress);
router.get("/list", clientController.findAll);
router.get("/edit/:id", clientController.findOne);
router.put("/edit/:id", clientController.update);
router.delete("/edit/:id", clientController.delete);
router.delete("/wp/:id", clientController.deleteWPClient);

module.exports = router;
