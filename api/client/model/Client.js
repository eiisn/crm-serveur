const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const clientSchema = mongoose.Schema({
    id_wp: {
        type: Number
    },
    first_name: {
        type: String,
    },
    last_name: {
        type: String,
    },
    date_created: {
        type: Date
    },
    email: {
        type: String,
    },
    address: {
        type: String
    },
    country: {
        type: String
    },
    city: {
        type: String
    },
    zip: {
        type: Number
    },
    phone: {
        type: String
    },
});

const Client = mongoose.model("Client", clientSchema);
module.exports = Client;
