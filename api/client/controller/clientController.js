const Client = require("../model/Client");
const WooCommerceAPI = require('woocommerce-api');

exports.getUsersWordpress = async (req, res) => {
    try {
        var WooCommerce = new WooCommerceAPI({
            url: 'https://bellevie-market.cloud-save.space',
            consumerKey: 'ck_75076b53a3706a6138b914006f94dfdb190d5b99',
            consumerSecret: 'cs_827d54c49267f4f261d5284a3578fda34e636ed0',
            wpAPI: true,
            version: 'wc/v2'
        });
        WooCommerce.getAsync('customers').then(function(result) {
            let json = JSON.parse(result.toJSON().body);
            for (let i in json) {
                const client = new Client({
                    id_wp: json[i].id,
                    first_name: json[i].first_name,
                    last_name: json[i].last_name,
                    password: json[i].password,
                    date_created: json[i].date_created,
                    email: json[i].email,
                    address: json[i].billing.address_1,
                    country: json[i].billing.country,
                    city: json[i].billing.city,
                    zip: json[i].billing.zip,
                    phone: json[i].billing.phone,
                });
                let data = client.save();
                res.status(201).json({ data });
            }
        });
    } catch (err) {
        res.status(400).json({ err: err });
    }
};

exports.findAll = async (req, res) => {
    try {
        Client.find().then(data => {
            console.log(data);
            res.send(data);
        })
            .catch(err => {
                res.status(500).send({
                    message:
                        err.message || "Some error occurred while retrieving tutorials."
                });
            });
    } catch (err) {
        res.status(400).json({ err: err });
    }
};

exports.findOne = (req, res) => {
    console.log(req)
    const id = req.params.id;

    Client.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found Tutorial with id " + id });
            else res.send(data);
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error retrieving Tutorial with id=" + id });
        });
};

exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    Client.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Tutorial with id=${id}. Maybe Tutorial was not found!`
                });
            } else
                res.send({ data, message: "Tutorial was updated successfully." });
        })
        .catch(err => {
            console.log(req.body)
            res.status(500).send({
                message: "Error updating Tutorial with id=" + id
            });
        });
};

exports.deleteWPClient = (req, res) => {
    const id = req.params.id;
    console.log(id)

    Client.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found Tutorial with id " + id });
            else
                console.log(data.id_wp)
                try {
                    var WooCommerce = new WooCommerceAPI({
                        url: 'https://bellevie-market.cloud-save.space',
                        consumerKey: 'ck_75076b53a3706a6138b914006f94dfdb190d5b99',
                        consumerSecret: 'cs_827d54c49267f4f261d5284a3578fda34e636ed0',
                        wpAPI: true,
                        version: 'wc/v3'
                    });
                    WooCommerce.delete("customers/"+data.id_wp, {
                        force: true
                    })
                        .then((response) => {
                            console.log(response.data);
                        })
                        .catch((error) => {
                            console.log(error.response.data);
                        });
                } catch (err) {
                    res.status(400).json({ err: err });
                }
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error retrieving Tutorial with id=" + id });
        });
}

exports.delete = (req, res) => {
    const id = req.params.id;

    Client.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete Tutorial with id=${id}. Maybe Tutorial was not found!`
                });
            } else {
                res.send({
                    message: "Tutorial was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Tutorial with id=" + id
            });
        });
};
